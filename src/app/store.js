import { configureStore } from '@reduxjs/toolkit';
import menuStateReducer from "../features/menuState";

export const store = configureStore({
  reducer: {
    menuState: menuStateReducer
  },
});

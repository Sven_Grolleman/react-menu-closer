import {createSlice} from "@reduxjs/toolkit";

export const menuStateSlice = createSlice({
    name: 'menuState',
    initialState: {
        value: false
    },
    reducers:{
        switchMenuState: state => {
            state.value = !state.value
            console.log(`is menu open: ${state.value}`);
        }
    }
})

export const {switchMenuState} = menuStateSlice.actions;
export default menuStateSlice.reducer;
import React from 'react';
import Button from "./Button";
import ButtonClass from "./ButtonClass";

const App = (props) => (
    (
        <div>
            <Button/>
            <ButtonClass/>
        </div>
    )
);

export default App;


import React from 'react';
import {switchMenuState} from "../features/menuState";
import {useDispatch, useSelector} from "react-redux";

const Button = () =>{
    const isOpen =  useSelector(state => state.menuState.value);
    const dispatch = useDispatch();

    return(
        <button onClick={()=> dispatch(switchMenuState())}>
            {isOpen? 'Close' : 'Open'}
        </button>
    )
}

export default Button;
import React, {Component} from 'react';
import {switchMenuState} from "../features/menuState";
import {connect} from "react-redux";

class ButtonClass extends Component {
    switchMenu = () => {
        this.props.switchMenuState();
    }

    render() {
        return (
            <button onClick={this.switchMenu}>
                {this.props.menuState.value? 'close' : 'open'}
            </button>
        );
    }
}

const mapStateToProps = state => {
    return{
        menuState: state.menuState
    }
}

export default connect(mapStateToProps, {switchMenuState})(ButtonClass);

